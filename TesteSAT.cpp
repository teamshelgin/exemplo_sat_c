#include "iostream"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dlfcn.h>
#include <vector>

using namespace::std;

const char* err;
void* lib;

char* c_atv = "123456789";
char* cnpj_sh = "16716114000172";
char* cnpj_ct = "14200166000166";
char* assinaturaCNPJ = "SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT";

string xmlvenda= "<?xml version=\"1.0\"?><CFe><infCFe versaoDadosEnt=\"0.07\"><ide><CNPJ>16716114000172</CNPJ><signAC>SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT</signAC><numeroCaixa>001</numeroCaixa></ide><emit><CNPJ>14200166000166</CNPJ><IE>111111111111</IE><IM>111111</IM><indRatISSQN>N</indRatISSQN></emit><dest><CPF>14808815893</CPF></dest><det nItem=\"1\"><prod><cProd>0000000000001</cProd><xProd>PRODUTO NFCE 1</xProd><NCM>94034000</NCM><CFOP>5102</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>3.51</vUnCom><indRegra>T</indRegra></prod><imposto><ICMS><ICMS00><Orig>0</Orig><CST>00</CST><pICMS>7.00</pICMS></ICMS00></ICMS><PIS><PISAliq><CST>01</CST><vBC>6.51</vBC><pPIS>0.0165</pPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>6.51</vBC><pCOFINS>0.0760</pCOFINS></COFINSAliq></COFINS></imposto></det><total><DescAcrEntr><vDescSubtot>0.51</vDescSubtot></DescAcrEntr><vCFeLei12741>0.56</vCFeLei12741></total><pgto><MP><cMP>01</cMP><vMP>6.51</vMP></MP></pgto><infAdic><infCpl>Trib aprox R$ 0,36 federal, R$ 1,24 estadual e R$ 0,00 municipal&lt;br&gt;CAIXA: 001 OPERADOR: ROOT</infCpl></infAdic></infCFe></CFe>";

string xmlRede = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><config><tipoInter>ETHE</tipoInter><tipoLan>DHCP</tipoLan></config>";

int SUCESSO = 0;

static const std::string base64_chars = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";


typedef char* (*ConsultarSAT)(int);
ConsultarSAT consultarSAT;

typedef char* (*ConsultarStatusOperacional)(int, char*);
ConsultarStatusOperacional consultarStatusOperacional;

typedef char* (*AtualizarSoftwareSAT)(int, char*);
AtualizarSoftwareSAT atualizarSWB;

typedef char* (*ExtrairLogs)(int, char*);
ExtrairLogs extrairLogs;

typedef int (*GeraNumeroSessao)();
GeraNumeroSessao numeroSessao;

typedef char* (*EnviarDadosVenda)(int, char*, char*);
EnviarDadosVenda venda;

typedef char* (*CancelarUltimaVenda)(int, char*, char*, char*);
CancelarUltimaVenda cancelar_venda;

typedef char* (*ConsultarNumeroSessao)(int, char*, int);
ConsultarNumeroSessao consulta_sessao;

typedef char* (*ConfigurarInterfaceDeRede)(int, char*, char*);
ConfigurarInterfaceDeRede configura_rede;

typedef char* (*AtivarSAT)(int, int, char*, char*, int);
AtivarSAT ativar;

typedef char* (*AssociarAssinatura)(int, char*, char*, char*);
AssociarAssinatura assinatura;

int valor=0;

void Programa();
void maquinaEstados(int opcao);

const vector<string> explode(const string& s, const char& c);

string base64_decode(string const& encoded_string);

static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

string base64_decode(string const& encoded_string) {
  int in_len = encoded_string.size();
  int i = 0;
  int j = 0;
  int in_ = 0;
  unsigned char char_array_4[4], char_array_3[3];
  std::string ret;

  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i ==4) {
      for (i = 0; i <4; i++)
        char_array_4[i] = base64_chars.find(char_array_4[i]);

      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
        ret += char_array_3[i];
      i = 0;
    }
  }

  if (i) {
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = base64_chars.find(char_array_4[j]);

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
  }

  return ret;
}

//int numeroSessao(){
//	return 1;
//}

const vector<string> explode(const string& s, const char& c)
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c) { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

int Load(){
        lib = dlopen("libdllsat.so",RTLD_LAZY);
        if(!lib){
		printf("Erro ao carregar Biblioteca: %s\n",dlerror());
		return 16;
        }

        //Consultar SAT
        consultarSAT = (ConsultarSAT)dlsym(lib,"ConsultarSAT");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }

	//Consultar Status Operacional
        consultarStatusOperacional = (ConsultarStatusOperacional)dlsym(lib,"ConsultarStatusOperacional");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }


        //Atualizar SWB SAT
        atualizarSWB = (AtualizarSoftwareSAT)dlsym(lib,"AtualizarSoftwareSAT");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }


        //Extrair logs SAT
        extrairLogs = (ExtrairLogs)dlsym(lib,"ExtrairLogs");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }

	//Gera numero sessao
        numeroSessao = (GeraNumeroSessao)dlsym(lib,"GeraNumeroSessao");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
       }
	
		//Enviar dados de venda
		venda = (EnviarDadosVenda)dlsym(lib,"EnviarDadosVenda");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }

		//cancelar venda
		cancelar_venda = (CancelarUltimaVenda)dlsym(lib,"CancelarUltimaVenda");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }

		//Consultar numero sessao
		consulta_sessao = (ConsultarNumeroSessao)dlsym(lib,"ConsultarNumeroSessao");
        err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }
		
		//Configurar Interface De Rede
		configura_rede = (ConfigurarInterfaceDeRede)dlsym(lib, "ConfigurarInterfaceDeRede");
		err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }

		//AtivarSAT
		ativar = (AtivarSAT)dlsym(lib,"AtivarSAT");
		err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }

		//associar assinatura
		assinatura = (AssociarAssinatura)dlsym(lib, "AssociarAssinatura");
		err = dlerror();
        if(err) {
                printf("Erro no dlsym: %s\n",err);
                return 16;
        }

        printf("Biblioteca carregada!");
	return SUCESSO;
}

void Programa(){
	int op = 2;
	printf("\n");
	system("clear");
	printf("[0]SAIR\n");
	printf("[1]Consultar SAT\n");
	printf("[2]Atualizar SoftwareSAT\n");
	printf("[3]Extrair LOG\n");
	printf("[4]Venda SAT\n");
	printf("[5]Consultar Sessão\n");
	printf("[6]Configurar Rede DHCP\n");
	printf("[7]Ativar SAT\n");
	printf("[8]Associar Assinatura SAT\n");
	printf("[9]Consultar Status Operacional\n");

	printf("\nESCOLHA O TESTE\n");
	scanf("%d", &op);
	
	maquinaEstados(op);
	
}

void maquinaEstados(int opcao){
string resp;
int count = 0;
int val;
switch (opcao){
	case 0:
		dlclose(lib);
		exit(0);
	break;
	case 1:
	{
		printf("==============CONSULTAR SAT======================\n");
		while(count < 10){
			resp = consultarSAT(numeroSessao());
			cout << resp.c_str() << "\n";
			count ++;
		}		
	}
	break;

	case 2:
	{
		printf("==============Atualizar SAT======================\n");
		resp = atualizarSWB(numeroSessao(), c_atv);
		cout << resp.c_str() << "\n";
	}
	break;

	case 3:
	{
		printf("==============Extrair log SAT======================\n");
		resp = extrairLogs(numeroSessao(), c_atv);
		vector <string> t = explode(resp, '|');
		
		if(strcmp(t[1].c_str(),"15000")!=0){
			cout << "Erro ao executar função\n";
			cout << t[1].c_str() << "\n";
			return;		
		}


		FILE* arq = fopen("log_sat.txt","a");
 
		resp = base64_decode(t[5]);
		cout << resp.c_str();
		fprintf(arq, "%s",resp.c_str());
		fclose(arq);		
		
	}
	break;
	
	case 4:
	{
		cout << "Funcao desabilitada\n";
		break;
		printf("=================Venda com cancelamento===================\n\nDigite a quantidade de teste:");
		scanf("%d", &val);
		count = 0;

		vector<string> dados;
		string canc;		
		//dados = explode(resp, '|');
		while(count < val){
			cout << "=================NOVA VENDA...["<<count<<"]=================\n";
			resp = venda(numeroSessao(), c_atv, (char*)xmlvenda.c_str());
			dados = explode(resp, '|');

			if(strcmp(dados[1].c_str(), "06000") == 0){
				system("tput setaf 2");
				cout << dados[8].c_str() << " - CHAVE DA VENDA\n";
				canc =  "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
						"<CFeCanc>"
						"	<infCFe chCanc=\"" + dados[8] + "\">"
						"		<ide>"
						"			<CNPJ>16716114000172</CNPJ>"
						"			<signAC>SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT</signAC>"
						"			<numeroCaixa>001</numeroCaixa>"
						"		</ide>"
						"		<emit></emit>"
						"		<dest />"
						"		<total/>"
						"	</infCFe>"
						"</CFeCanc>";				
				valor = numeroSessao();				

				resp = cancelar_venda(valor , c_atv,(char*)dados[8].c_str(),  (char*)canc.c_str());
				dados = explode(resp, '|');
				if(strcmp(dados[1].c_str(), "07000") != 0){
					system("tput setaf 1");
				}
				cout << dados[1].c_str() << " - " << dados[3].c_str() <<" - RETORNO CANCELAMENTO VENDA\n\n";
				system("tput sgr0");
			}else{
				cout << "ERRO NA VENDA\n";
				cout << resp.c_str() << "\n";
			}
			count ++;
		}
	}
	break;

	case 5:
	{
	printf("=================Consultar numero sessão===================\n\nDigite a sessão:");
		scanf("%d", &val);
		printf("\n %d \n", val);
		resp = consulta_sessao(numeroSessao(), c_atv, val);
		cout << resp.c_str() << "\n";
	}
	break;

	case 6:
	{
		printf("=================Configurar interface de rede DHCP===================\n\n");
		resp = configura_rede(numeroSessao(), c_atv, (char*)xmlRede.c_str());
		cout << resp.c_str() << "\n";
	}
	break;

	case 7:
	{
		cout << "Funcao desabilitada\n";
		break;
		printf("=================Ativação SAT===================\n\n");
		resp = ativar(numeroSessao(), 1, c_atv, cnpj_ct, 35);
		cout << resp.c_str() << "\n";
	}
	break;

	case 8:
	{
		cout << "Funcao Desabilitada\n";
		break;
		printf("=================Vinculação do SAT===================\n\n");
		char cnpjs[27];
		strcat(cnpjs, cnpj_sh);
		strcat(cnpjs, cnpj_ct);	
		resp = assinatura(numeroSessao(), c_atv, cnpjs, assinaturaCNPJ);
		cout << resp.c_str() << "\n";
	}
	break;

	case 9:
	{
		printf("==============CONSULTAR STATUS OPERACIONAL======================\n");
		while(count < 1){
			resp = consultarStatusOperacional(numeroSessao(), c_atv);
			cout << resp.c_str() << "\n";
			count ++;
		}		
	}
	break;


	default:	
		printf("==============OPÇÃO INVALIDA==============\n");
		system("echo Pressione qualquer tecla para continuar...; read dummy;");
	break;
	}
	system("echo Pressione enter para continuar...; read dummy;");
	Programa();
return;
}

int main(int argc, char *argv[])
{
	Load();
	Programa();
	return 0;
}

